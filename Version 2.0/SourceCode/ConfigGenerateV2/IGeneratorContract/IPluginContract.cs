﻿using System;
using System.Collections.Generic;

namespace IGeneratorContract
{
    public interface IPluginContract
    {
        IExcelModel ExcelInfo { get; set; }
        string PluginName { get; }
        string WarningConfirmText { get; }
        bool Execute(string outputPath, out string msg);
    }
}
