﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using IGeneratorContract;
using ConfigGeneratorV2UI.Config;

namespace ConfigGeneratorV2UI
{
    public partial class EntranceUI : Form
    {
        public EntranceUI()
        {
            InitializeComponent();
            InitMenuStrip();
        }

        public Dictionary<string, IPluginContract> Tools = new Dictionary<string, IPluginContract>();
        public string ExcelFilePath { get; set; }

        private void InitMenuStrip()
        {
            string pluginPath = Path.Combine(Directory.GetCurrentDirectory(), "Plugin");
            if (!Directory.Exists("Plugin"))
            {
                Directory.CreateDirectory(pluginPath);
                return;
            }
            string[] dll = Directory.GetFiles(pluginPath, "*.dll", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < dll.Length; i++)
            {
                var a = Assembly.LoadFile(dll[i]);
                foreach (Type t in a.GetTypes())
                {
                    if (typeof(IPluginContract).IsAssignableFrom(t))
                    {
                        ShowMenuStrip(a.CreateInstance(t.FullName) as IPluginContract);
                    }
                }
            }
        }

        private void ShowMenuStrip(IPluginContract c)
        {
            ToolStripMenuItem item = new ToolStripMenuItem();
            item.Text = c.PluginName;
            item.Click += ToolItemOnClick;
            item.Name = Tools.Count.ToString();
            Tools.Add(Tools.Count.ToString(), c);

            currentMenuStrip.Items.Add(item);
        }

        private void ToolItemOnClick(object sender, EventArgs e)
        {
            var a = sender as ToolStripMenuItem;

            var b = MessageBox.Show(Tools[a.Name].WarningConfirmText, "Warning !", MessageBoxButtons.OKCancel);

            if (b == System.Windows.Forms.DialogResult.OK)
            {
                string msg = string.Empty;
                if (Tools[a.Name].Execute(ExcelFilePath, out msg))
                {
                    MessageBox.Show("Execute Success !! \n\n" + msg);
                }
                else { MessageBox.Show("Execute Failure !! \n\n" + msg); }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Setting.Instance.AUTHOR_INFO);
        }

        private void EntranceUI_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files.Length == 1)
                {
                    if (files[0].EndsWith("xls", true, null) || files[0].EndsWith("xlsx", true, null))
                    {
                        if (files[0] != ExcelFilePath)
                        {
                            ExcelFilePath = files[0];
                            string msg = string.Empty;

                            //Read File 
                            ExcelSetting.Instance.ExcelInfo = new ExcelReader.ExcelReaderProcess(files[0]).GetExcelInfo(out msg);

                            //Set Plugin's Excel Infomation
                            foreach (var item in Tools.Values)
                            {
                                item.ExcelInfo = ExcelSetting.Instance.ExcelInfo;
                            }
                            MessageBox.Show("Infomation" + "\n" + files[0] + "\n" + msg);
                        }
                        return;
                    }
                }
            }
        }
    }
}
