﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using IGeneratorContract;
using ConfigGeneratorV2UI.Config;
using System.Collections.Generic;

namespace ConfigGeneratorV2UI.ExcelReader
{
    public class ExcelReaderProcess : IDisposable
    {
        private string fileName = null;
        private IWorkbook workbook = null;
        private FileStream fs = null;
        public int cellCount;
        public int rowCount;

        private IExcelModel ExcelInfo { get; set; }

        public ExcelReaderProcess(string fileName)
        {
            this.fileName = fileName;
            ExcelInfo = new ExcelModel();
        }

        public IExcelModel GetExcelInfo(out string msg)
        {
            msg = "";

            try
            {
                this.fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);

                if (fileName.IndexOf(".xlsx") > 0)
                    this.workbook = new XSSFWorkbook(this.fs);
                else if (fileName.IndexOf(".xls") > 0)
                    this.workbook = new HSSFWorkbook(this.fs);
                else return null;

                //get default Sheet where index is zero
                ISheet sheet = this.workbook.GetSheetAt(0);
                if (sheet == null) return null;
                int num = 0;

                //This row is title row 
                IRow row = sheet.GetRow(0);

                cellCount = row.LastCellNum;
                rowCount = sheet.LastRowNum;

                //Get title type is string
                for (int i = (int)row.FirstCellNum; i < this.cellCount; i++)
                {
                    ICell cell = row.GetCell(i);
                    if (cell != null)
                    {
                        string stringCellValue = cell.StringCellValue;
                        if (!string.IsNullOrWhiteSpace(stringCellValue))
                        {
                            if (ExcelInfo.ColumnName.Contains(stringCellValue))
                            {
                                msg = string.Format("The Lsit Has The Same Column Title ,Name '{0}' ",stringCellValue);
                                return null;
                            }
                            ExcelInfo.ColumnName.Add(stringCellValue);
                        }
                        else
                            ExcelInfo.ColumnName.Add(string.Format("Column{0}", i));
                    }
                }

                //num = sheet.FirstRowNum + 1;
                row = sheet.GetRow(1);

                //Get types as list
                for (int i = row.FirstCellNum; i < cellCount; i++)
                {
                    ICell cell = row.GetCell(i);
                    if (cell != null)
                    {
                        string stringCellValue = cell.StringCellValue.ToUpper();
                        if (!string.IsNullOrWhiteSpace(stringCellValue))
                        {
                            switch (stringCellValue)
                            {
                                case "STRING":
                                    ExcelInfo.ColumnType.Add(typeof(string));
                                    break;
                                case "FLOAT":
                                    ExcelInfo.ColumnType.Add(typeof(float));
                                    break;
                                case "INT":
                                    ExcelInfo.ColumnType.Add(typeof(int));
                                    break;
                                case "STRING[]":
                                    ExcelInfo.ColumnType.Add(typeof(string[]));
                                    break;
                                case "FLOAT[]":
                                    ExcelInfo.ColumnType.Add(typeof(float[]));
                                    break;
                                case "INT[]":
                                    ExcelInfo.ColumnType.Add(typeof(int[]));
                                    break;
                                default:
                                    msg = string.Format("Cant find the Type '{0}'  ,  Please check the document  : ) ", stringCellValue);
                                    return null;
                            }
                        }
                        else
                        {
                            ExcelInfo.ColumnType.Add(typeof(string));
                        }
                    }
                }

                num = sheet.FirstRowNum + 2;

                for (int i = num; i <= this.rowCount; i++)
                {
                    IRow row2 = sheet.GetRow(i);
                    if (row2 != null)
                    {
                        List<string> items = new List<string>();
                        for (int j = (int)row2.FirstCellNum; j < this.cellCount; j++)
                        {
                            if (row2.GetCell(j) != null)
                            {
                                items.Add(row2.GetCell(j).ToString());
                            }
                        }
                        ExcelInfo.Rows.Add(i, items);
                    }
                }

                return ExcelInfo;
            }
            catch (Exception ex)
            {
                msg = "Exception: " + ex.Message;
            }

            return null;
        }


        public void Dispose()
        {
            if (this.fs != null)
            {
                this.fs.Close();
            }
            fs = null;
            GC.SuppressFinalize(this);
        }
    }
}
