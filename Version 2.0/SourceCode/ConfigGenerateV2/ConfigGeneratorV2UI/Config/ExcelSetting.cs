﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IGeneratorContract;

namespace ConfigGeneratorV2UI.Config
{
    public class ExcelSetting
    {
        private ExcelSetting() { }

        private static ExcelSetting instance;
        public static ExcelSetting Instance
        {
            get { return instance ?? (instance = new ExcelSetting()); }
        }

        public IExcelModel ExcelInfo { get; set; } 
        public string ExcelPath { get; set; }
    }
}
