﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IGeneratorContract;

namespace ConfigGeneratorV2UI.Config
{
    public class ExcelModel : IExcelModel
    {
        public ExcelModel()
        {
            ColumnType = new List<Type>();
            ColumnName = new List<string>();
            Rows = new Dictionary<int, List<string>>();
        }

        #region IExcelModel Members

        public List<Type> ColumnType
        {
            get;
            set;
        }

        public List<string> ColumnName
        {
            get;
            set;
        }

        public Dictionary<int, List<string>> Rows
        {
            get;
            set;
        }

        #endregion
    }
}