﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace ConfigGeneratorV2UI.Config
{
    public class Setting
    {
        private Setting() { }

        private static Setting instance;
        public static Setting Instance
        {
            get { return instance??(instance = new Setting()); }
        }

        public void Init()
        {
            AUTHOR_INFO = ConfigurationSettings.AppSettings["AUTHOR_INFO"];
        }

        public string AUTHOR_INFO { get; set; }

    }
}
