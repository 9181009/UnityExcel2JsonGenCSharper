﻿using IGeneratorContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Class1 : IPluginContract
    {
        public bool Execute(string path, out string msg)
        {
            if (ExcelInfo == null)
            {
                msg = "error Log: ExcelInfo Is Null";
            }
            else
            {
                msg = "error Log" + ExcelInfo.ColumnName[0];
            }

            return true;
        }

        public string PluginName
        {
            get
            {
                return "测试";
            }
        }

        public string WarningConfirmText
        {
            get
            {
                return "你确认已经加载过文件吗？";
            }
        }

        public IExcelModel ExcelInfo { get; set; }
    }
}
